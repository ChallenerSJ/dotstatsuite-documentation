---
title: "Installing .Stat Suite from containers"
subtitle: 
comments: false
weight: 50

---

The following pages provide information about the ready-to-use .Stat Suite Docker images, which can be freely re-used to easily compose a new topology (system architecture) by anyone in their own cloud or premises.  

This approach requires knowledge of the **[Docker](https://docs.docker.com/install/overview/)** technology and orchestration such as **[Kubernetes](https://kubernetes.io/docs/home/)** technology.  

You can also have a look at the [Delivery and support streams' diagram](https://sis-cc.gitlab.io/dotstatsuite-documentation/getting-started/) for a better understanding of the installation approach.
