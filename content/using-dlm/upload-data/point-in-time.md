---
title: "Point in Time release"
subtitle: 
comments: false
weight: 300

---

*under construction...*

Note that there are currently no timezone conversions for the Point-in-Time (PIT) dates, and that the user's date time might not be the same than the server's date time. **When specifying a Point-in-Time (PIT) date, please use the server's timezone.**