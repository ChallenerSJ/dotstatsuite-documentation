---
title: "Facets"
subtitle: 
comments: false
weight: 1800

---

#### Table of Content
- [What information is presented as facets](#what-information-is-presented-as-facets)
- [How facets are used](#how-facets-are-used)
- [Used filters](#used-filters)
- [Pinned facets](#pinned-facets)

---

### What information is presented as facets
**Data source**: If configured so (relates to *installation/configuration*), one facet is returned for the **data source** (SDMX endpoint used to retrieve dataflow information).  

**One facet per localised CategoryScheme**: The localised CategoryScheme(s) in which the dataflow is categorised.  
**Note** that each dataflow can be attached to one or more than one category.  
The CategoryScheme can have a simple hierarchy (each child element can have no or only one parent element defined) of an undefined depth (usually not more than 3 or 4), e.g.:

![de facet hierarchy](/dotstatsuite-documentation/images/de-facet-1.png)

**One facet per concept that is used to define dataflow dimensions**: Concepts are distinguished only by their localised concept names (per language), not by their IDs.  
**Note** that all Concepts having the same localised name (independently from their ID) are grouped into one single facet.  

**Special concept facet for Frequency**.  
A dataflow can contain data for one or more frequencies that are normally defined through a frequency dimension. The specificity is that some dataflows with data of only one frequency might not have a frequency dimension, but it is also possible to find those dataflows through the frequency facet.  
In order to determine the frequency of data in such dataflows and add a relevant facet value for these, the search service do an additional unfiltered data query for one observation value.  
**Note** that if a dataflow has no time dimension, then it will also not have a value for the frequency facet.  

**Special concept facet for Time period**  
Dataflows do not have a codelist for the time period dimension (time dimension). Therefore, only the actual content constraint can be used to determine the available time periods and thus the values of the time period facet.  
For practical reasons, the search only allows a filter by **ANNUAL** time periods. All non-annual time periods are converted to annual time period facet values. The time period facet is built as a range facet, because the facet filtering is done through a start year and an end year.  
**Note** that if a dataflow has no time dimension, then it will also not have values for the time period facet.  

#### Indexation content restrictions

A dataflow is indexed **only if** there is data associated to it.  
The data availability check is based on the `Actual Content Constraint` attached to the dataflow. The dataflow is indexed only if there is:
- a non-empty Actual Content Constraint
- no Actual Content Constraint (for compatibility with SDMX web services not based on .Stat Suite).

A particular dimension of a dataflow is indexed only if the dimension values with available data do not exceed the limit defined in the SFS configuration parameter `DIMENSION_VALUES_LIMIT`, which is by default set to 1000. It protects the search engine from too big codelists and prevents performance impacts. For more information see [here](https://sis-cc.gitlab.io/dotstatsuite-documentation/configurations/de-configuration/#limit-for-indexing-dimensions-per-dataflow).  

Dimension values of a dataflow are indexed only if there are data available for the values or, if those values are hierarchical parents in case their children values have data. For that purpose, the search indexing takes the current `Actual Content Constraint` of the dataflow, if available, into account.

---

### How facets are used
#### Homepage facets
The localised .Stat DE home page presents a combination of a free text search box and a list of any few facets specifically defined in the [configuration](https://sis-cc.gitlab.io/dotstatsuite-documentation/configurations/de-configuration/) (in the currently chosen language) of the faceted search service.  

![de homepage facet](/dotstatsuite-documentation/images/de-facet-2.png)

Each facet, when opened, presents a selectable root-level of facet values together with a non-selectable second-level facet values. The root-level and second-level facet values represent the available values for all dataflows currently being indexed.  

> **Note** that, since the [August 25, 2020 Release .Stat Suite JS 5.3.0](https://sis-cc.gitlab.io/dotstatsuite-documentation/changelog/#august-25-2020) release, it is possible to make the individual second-level homepage facet values clickable by configuration. See the documentation [here](https://sis-cc.gitlab.io/dotstatsuite-documentation/configurations/de-configuration/#selectable-second-level-homepage-facet-values).

The free text search and the navigation through pre-defined facets are exclusive and result systematically in a new search discarding any previously made search selections.

#### Facets on the search result page
The facets shown in the search result page are fully dependent on the current search context.  
Common facet dimensions can be automatically removed (hidden) from the search result page when specifically defined in the [configuration](https://sis-cc.gitlab.io/dotstatsuite-documentation/configurations/de-configuration/).

![de facet result](/dotstatsuite-documentation/images/de-facet-3.png)

#### Facet types
All facets are **multi-selection** facets (an existing facet value selection does not prevent selecting other still available facet values) **except** for the **Time Period** facet. The Time Period facet is a range selection facet that shows a start year and end year independently from the currently selected frequency.  

![de facet types](/dotstatsuite-documentation/images/de-facet-4.png)

#### Facet information
The facet header contains the number of facet values available, and the number of currently selected facets (green numbers). The facet values show a number indicating the number of corresponding search results. Parent values contain the number of results valid for themselves and for all of their children.  

![de facet information](/dotstatsuite-documentation/images/de-facet-5.png)

#### Hierarchical contents
In case of a hierarchy in the dimension items, the facet displays the root parents’ list at first. A blue arrow next to an item and right-aligned indicates when this item is a parent of sub-item(s).  
If some of the root parents have children, then by clicking on the arrow, the children of this root parent will be displayed instead. The same behaviour is applied if some of the children also have a sub-children list.

![de facet types](/dotstatsuite-documentation/images/de-facet4bis.png)

#### Data availability

In hierarchical search facets, parent values, for which any of the resulting dataflows do not have data, are not selectable and are marked in light grey colour. Still, the user can navigate to the children and back to the parent again.

![de facet types](/dotstatsuite-documentation/images/de-facet5bis.png)

---

### Used filters
The "Used filters" area in search resuslts displays all current selected items, per facet.  
The top right *green* numbering feature counts the total number of selected items for all facets.  
Used filters are ranked and displayed by facet type.  
Users can unselected:
* one single item by clicking on the `x` next to the item label, or
* all items for a given dimension by clicking on the `x` next to the dimension label, or finally 
* all selections by clicking on `Clear all filters`. When all filters are unselected, then the user is brought back to the home page.

![de used filters](/dotstatsuite-documentation/images/de-facet-6.png)

---

### Pinned facets
Facets displayed with a [**.**] after their localised name are called **pinned facets** and are configured (the [configuration](https://sis-cc.gitlab.io/dotstatsuite-documentation/configurations/de-configuration/#search-results-page-pinned-facets) is set per DE instance) to always be displayed, when available, at the first top position(s) in the list.  
In addition, a help [**?**] tooltip is shown right next to the Filters header title, which displays the following information (localised): *"Filters marked with **.** are, when available, always listed first."*

![Pinned facets](/dotstatsuite-documentation/images/de-pinned-facets.png)
