---
title: "Charts"
subtitle: 
comments: 
weight: 3000

---

All charts use the current selection, which can be modified using the filters.  
All charts are customisable, using the common Customise menu.  
All charts are downloadable in png format.  

The charts share a common header and footer with the preview-table. More information is available [here](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-de/viewing-data/common-header-and-footer/).

The following types of charts are supported:  
![Toolbar](/dotstatsuite-documentation/images/de-toolbar2.png)

---

### Bar chart
**Example**  

![Toolbar](/dotstatsuite-documentation/images/bar.png)

---

### Row chart
**Example**  

![Toolbar](/dotstatsuite-documentation/images/row.png)

---

### Stacked bar chart
**Example**  

![Toolbar](/dotstatsuite-documentation/images/stacked-bar.png)

---

### Scatter plot chart
**Example**  

![Toolbar](/dotstatsuite-documentation/images/scatter.png)

---

### Horizontal symbol chart
**Example**  

![Toolbar](/dotstatsuite-documentation/images/horiz-symbol.png)

---

### Vertical symbol chart
**Example**  

![Toolbar](/dotstatsuite-documentation/images/vertic-symbol.png)

---

### Timeline chart
**Example**  

![Toolbar](/dotstatsuite-documentation/images/timeline.png)

---

### Choropleth map
**Example**  

![Toolbar](/dotstatsuite-documentation/images/map.png)

