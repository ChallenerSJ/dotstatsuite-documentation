---
title: "Common header & footer for all views"
subtitle: "Applies to both preview-tables and charts"
comments: 
weight: 2050

---

#### Table of content
- [Visualisation header](#visualisation-header)
- [Visualisation footer](#visualisation-footer)

---

### Visualisation header
 
The visualisation header (common for both table and chart views) consists of:  
- Title
- First subtitle
- Second subtitle

#### Title

The title is generated automatically and cannot be changed manually. It contains the name of the dataflow in the currently selected language. If the dataflow name is not available in that language, then the dataflow ID within squared brackets is shown. The dataflow name responds to the [label customisation](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-de/viewing-data/toolbar/#labels).

A footnote icon for additional information (attributes) at the end of the title is displayed if a footnote exists at the dataflow level. 

**Example:**  

**Key Short-Term Economic Indicators** * 

#### First subtitle

The table first subtitle consists of the concatenated names of unique(ly selected) dimension items (without unit_measure concepts). All dimensions with exactly one selected item (or where the dimension has only one item) are used for that (and line-wrapped if required). The item names are preceeded by the related dimension names and separated from each other by spaces and the special dot character: " ● ". For any lower level item in the corresponding dimension hierarchy, the FULL_NAME alternative (containing the hierarchical context) should be used instead of the name. The names are displayed in the currently selected language. If such names are not available, then they are replaced by the corresponding IDs. The names respond to the [label customisation](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-de/viewing-data/toolbar/#labels).

A footnote icon for additional information is displayed at the end of a dimension item or at the end of the subtitle (depending on the attachment of the underlying attribute value). 

**Example:**  

***Measure:*** Growth previous period * ● ***Frequency:*** Annual ● ***Time:*** 2015 *  

The subtitle includes additional information on the data shown in the visualisation (UPR attributes). If the UPR attributes apply to all the data in the visualisation, then the UPR concatenation is shown in the subtitle. Example: Euro, thousands, 2015.  
The subtitle is generated automatically and cannot be changed manually.

#### Second subtitle

The table second subtitle consists of the concatenated, comma-separated names of unique UNIT_MEASURE dimension or attribute items (and line-wrapped if required). The second subtitle is preceeded by the virtual dimension name "Unit of measure: ". The names are displayed in the currently selected language. If such names are not available, then they are replaced by the corresponding IDs. The names respond to the [label customisation](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-de/viewing-data/toolbar/#labels).

**Example:**  

***Unit of measure:*** US dollar, millions

#### Complete example

**FDI financial flows - Main aggregates**  
***Measurement principle:*** Asset liability principle ● ***Type of entity:*** All resident units ● ***Type of FDI:*** FDI financial flows ● ***FDI component:*** Total direct investment ● ***Accounting entry:*** Assets  
***Unit of measure:*** US dollar, millions  

---

### Visualisation footer
 
The visualisation footer (common for both table and chart views) consists of:
- configurable copyright "©" link to the web site's "Terms & Conditions";
- autogenerated source name (dataflow name) linking to the default visualisation page for the current dataflow. The dataflow name responds to the [label customisation](https://sis-cc.gitlab.io/dotstatsuite-documentation/using-de/viewing-data/toolbar/#labels);
- configurable owner logo.
